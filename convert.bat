@echo off
setlocal EnableDelayedExpansion

rem Input and output folder are in same directory as batch file.
set "InputFolder=%~dp0media"
set "OutputFolder=%~dp0output"
echo  Input folder is: %InputFolder%
echo Output folder is: %OutputFolder%
echo.

rem Search in input folder and all subfolders for the specified types
rem of audio files and output what is found with the output file name
rem in output folder. Delayed environment variable expansion is needed
rem for all variables set or modified within the body of the FOR loop.

for /R "%InputFolder%" %%I in (*.aac *.ac3 *.wav *.mp4) do (
    set "InputFileName=%%~nxI"
    set "InputFilePath=%%~dpI"
    set "OutputFileName=%%~nI.mp3"
    set "OutputFilePath="!InputFilePath:~0,-1!"
    echo  Input file name is: !InputFileName!
    echo  Input file path is: !InputFilePath!
    echo Output file name is: !OutputFileName!
    echo Output file path is: !InputFilePath!
    ffmpeg -i "!InputFilePath!!InputFileName!" -n -codec:a libmp3lame -b:a 192k "!InputFilePath!!OutputfileName!"
    echo --------
)

endlocal
echo.
pause
rem Exit batch processing and return to command process.
exit /B

rem Subroutine to get last folder name from first argument and append an
rem underscore, the file name of the input file without file extension
rem passed as second argument and the file extension MP3. But if the path
rem to the file is identical with input folder path, get just name of file
rem with different file extension.

rem Exit subroutine.
exit /B