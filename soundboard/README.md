# [2wice.net Soundboard](https://2wice.net/soundboard)

TWICE soundboard (along with video for most of them) using HTML5 audio and video.

Uses [Skeleton](http://getskeleton.com/), [jQuery](https://jquery.com/), [jQueryUI](https://jqueryui.com/), [AngularJS](https://angularjs.org/)
