//console.log("counter.js loaded");

//grab url path for cookie name
var path = window.location.pathname

$(document).ready(function() {
	checkCookie("plays");
	//counter
	$("video").bind("ended", function() {
		//console.log("ended");
		//increment count
		count++;
		//console.log("count++");
		//update count text
		$("#counter").html(count);
		//save count to cookie
		setCookie("plays", count);
		//repeat
		this.play();
	});
});


//check if cookies, if none set to 0
function checkCookie() {
	//console.log("cookie check");
	count = getCookie("plays");
	if (count != "") {
		//console.log("cookies found");
		$("#counter").html(count);
	}
	else {
		//console.log("cookies not found");
		count = 0;
	}
};


//set cookie
function setCookie(cname, cvalue) {
	//console.log(path);
	//console.log("cookie set " + cvalue);
    document.cookie = cname + "_" + path + "=" + cvalue + "; path=/";
};

//get cookie (  copypasta :(  )
function getCookie(cname) {
    var name = cname + "_" + path + "=";
    var ca = document.cookie.split(';');
    //console.log(ca);
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}