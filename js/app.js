var app = angular.module('soundboard', []);

app.controller('MainCtrl', function($scope) {
	//all the shit in json
	var json = {
		"members": [
		//nayeon
			{
				"name":"nayeon",
				"files": [
					{"title":"pasta"},
					{"title":"haha"},
					{"title":"laugh"},
					{"title":"laugh 2"},
					{"title":"unbelievable"},
					{"title":"ba ba ba"},
					{"title":"come on"},
					{"title":"yo"},
					{"title":"yo 2"},
					{"title":"ooh"},
					{"title":"ooh ooh"},
					{"title":"ahh"},
					{"title":"bang"},
					{"title":"ice cream"},
					{"title":"you know"},
					{"title":"banana"},
					{"title":"ahh 2"},
					{"title":"good morning"}
					]
			},
		//jeongyeon
		{
				"name":"jeongyeon", 
				"files": [
					{"title":"double kill"},
					{"title":"haha"},
					{"title":"orange"},
					{"title":"blueberry"},
					{"title":"mannequin"},
					{"title":"no"},
					{"title":"wooh"},
					{"title":"yeah"},
					{"title":"how dare you"},
					{"title":"laugh"},
					{"title":"laugh 2"},
					{"title":"ahh"},
					{"title":"ahh 2"},
					{"title":"scream"}
					]
			},
		//momo
		{
				"name":"momo", 
				"files": [
					{"title":"momo scream 1"},
					{"title":"momo scream 2"},
					{"title":"momo scream 3"},
					{"title":"momo scream 4"},
					{"title":"momo scream 5"},
					{"title":"laugh"},
					{"title":"laugh 2"},
					{"title":"laugh 3"},
					{"title":"laugh 4"},
					{"title":"laugh 5"},
					{"title":"laugh 6"},
					{"title":"multi kill"},
					{"title":"apple"},
					{"title":"no"},
					{"title":"why"},
					{"title":"bye"},
					{"title":"cheer up"},
					{"title":"cheer up 2"},
					{"title":"da da da"},
					{"title":"fighting"},
					{"title":"good morning"},
					{"title":"i'm so sorry"},
					{"title":"it's a small world"},
					{"title":"ooh ahh"},
					{"title":"oppa"},
					{"title":"so cute"},
					{"title":"a"},
					{"title":"eh"},
					{"title":"eh 2"},
					{"title":"how dare you"},
					{"title":"i want to go in"},
					{"title":"so what"},
					{"title":"pink lamborghini"},
					{"title":"huh"}
					]
			},
		//sana
		{
				"name":"sana", 
				"files": [
					{"title":"creamy"},
					{"title":"it's so creamy oh yeah"},
					{"title":"call me"},
					{"title":"fantastic"},
					{"title":"bang"},
					{"title":"strawberry"},
					{"title":"mmm"},
					{"title":"shy shy shy"},
					{"title":"ahh"},
					{"title":"ohh"},
					{"title":"flowers"},
					{"title":"laugh"},
					{"title":"this is so nice"},
					{"title":"a b"},
					{"title":"bang bang"},
					{"title":"yaa"}
					]
			},
		//jihyo
		{
				"name":"jihyo", 
				"files": [
					{"title":"fire in the hole"},
					{"title":"ultra kill"},
					{"title":"honey bread"},
					{"title":"mcdonalds"},
					{"title":"jihyo scream"},
					{"title":"banana"},
					{"title":"mnet"},
					{"title":"ready"},
					{"title":"action"},
					{"title":"cut"},
					{"title":"yo"}
					]
			},
		//mina
		{
				"name":"mina", 
				"files": [
					{"title":"ice cream"},
					{"title":"cheese cake"},
					{"title":"b site"},
					{"title":"crazy kill"},
					{"title":"anyong"},
					{"title":"good morning"},
					{"title":"huh"},
					{"title":"laugh"},
					{"title":"laugh 2"},
					{"title":"mmm"},
					{"title":"morning mina"},
					{"title":"ohayo gozaimasu"},
					{"title":"shy shy shy"},
					{"title":"shy shy shy 2"},
					{"title":"shy shy shy 3"},
					{"title":"oppa"}
					]
			},
		//dahyun
		{
				"name":"dahyun", 
				"files": [
					{"title":"laugh"},
					{"title":"god send"},
					{"title":"pineapple"},
					{"title":"hold on x1000"},
					{"title":"pbpbpb"}
					]
			},
		//chaeyoung
		{
				"name":"chaeyoung", 
				"files": [
					{"title":"great kill"},
					{"title":"nice shot"},
					{"title":"growl"},
					{"title":"blueberry"},
					{"title":"laugh"},
					{"title":"laugh 2"},
					{"title":"laugh 3"},
					{"title":"ahh"},
					{"title":"wooh"},
					{"title":"mnet"},
					{"title":"it's me"},
					{"title":"penguin"}
					]
			},
		//tzuyu
		{
				"name":"tzuyu", 
				"files": [
					{"title":"stress"},
					{"title":"$10"},
					{"title":"a"},
					{'title':"aaa"},
					{"title":"haha"},
					{"title":"headshot"},
					{"title":"i'm lovin' it"},
					{"title":"pear"},
					{"title":"ahh"},
					{"title":"cheer up"},
					{"title":"cheer up 2"},
					{"title":"english"},
					{"title":"my english is not good"},
					{"title":"fighting"},
					{"title":"good morning"},
					{"title":"gwiyomi"},
					{"title":"hello"},
					{"title":"i'm sorry"},
					{"title":"okay"},
					{"title":"ooh ahh"},
					{"title":"oppa"},
					{"title":"r u ok"},
					{"title":"showcase"},
					{"title":"so cute"},
					{"title":"unnie"},
					{"title":"ahh 2"},
					{"title":"1 2 3 4"},
					{"title":"penguin"}
					]
			},
		]
	}
	$scope.twice = json;
})