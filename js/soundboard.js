$(document).ready(function() {
	console.log('hi whats up');
	//video shown
	shown = false;
	//grab url path for cookie name
	//var path = window.location.pathname
	//check video pref on load from cookie
	//checkCookie();
	video = false;
	//loop status
	loop = false;
	//click function
	$(".play").click(function() {
		var $this = $(this);
		//show active file playing
		if (video) {
			$(".play").removeClass('button-primary');
			$(this).addClass('button-primary');
		}
		makePath($this);
	});

	//construct path to media file
	function makePath($this) {
		var fileName = $($this).text();
		var member = $($this).attr("id");
		//build video path
		var filePath = "../media/" + member + "/" + fileName + ".mp4";
		lastPlayed = filePath
		if (video) {
			UrlExists(filePath, member, fileName);
		}
		else {
			makeAudioPath(member, fileName)
		}

		//if ( video && UrlExists(filePath) ) { // if video enabled AND video exists, build video path
		//	//update video
		//	updateVideo(filePath);
		//}
		//else { // if video disabled, build mp3 path, also fallback if video does not exist (for some)
		//	var filePath = "../media/" + member + "/" + fileName + ".mp3";
		//	playAudio(filePath);
		//	//clear active buttons
		//	$(".play").removeClass('button-primary');
		//}
	};


	//CHECK IF VIDEO EXISTS
	function UrlExists(filePath, member, fileName) {
		//depreciated XMLHttpRequest so use ajax instead 

		//var http = new XMLHttpRequest();
		//http.open('HEAD', url, false);
		//http.send();
		//return http.status!=404;
		$.ajax({
			type: 'HEAD',
			url: filePath,
			success: function() {
				updateVideo(filePath);
			},  
			error: function() {
				makeAudioPath(member, fileName)
			}
		});
	}

	function makeAudioPath(member, fileName) {
		var filePath = "../media/" + member + "/" + fileName + ".mp3";
		playAudio(filePath);
		//clear active buttons
		$(".play").removeClass('button-primary');
	}

	//VIDEO MODIFERS
	function updateVideo(filePath){
		//update video source
		$("video #source").attr("src", filePath);
		//load video
		$("#video")[0].load();
		//show video if hidden
		if (!shown) {
			setHeight();
			//show vid
			$("#hideVideo").slideDown(400);
			shown = true;
		}
	};
	//fix slideDown jump - set height before sliding down
	function setHeight() {
		var width = $(".container").width();
		var height = width * 0.5625; //16:9 aspect ratio
		//console.log("height" + height);
		//set height
		$("#hideVideo").height(height);
	};

	//replay video on click (if loop is disabled)
	$("#video").click(function() {
		$(this)[0].play();
	});

	//update audio source and play file
	function playAudio(filePath) {
		var audioFile = new Audio(filePath);
		audioFile.play();
	};

	//TOGGLES

	//toggle between video or mp3
	$("#videoToggle").click(function() {
		videoToggle();
	});

	//toggle videos
	function videoToggle() {
		if (video) { //change to mp3
			//change button to on
			$("#videoToggle").addClass('button-primary').text("Enable Video (slower)");
			$("#copy").text("Copy last played mp3 URL")
			video = false;
			//clear active buttons
			$(".play").removeClass('button-primary');
			if (shown) {//if video already showing, hide it
				$("#hideVideo").slideUp(400);
				shown = false;
			}
			//stop current video
			$("#video").get(0).pause();
			//setCookie("videoPref", "false");
		}

		else {//change to video
			//change button to off
			$("#videoToggle").removeClass('button-primary').text("Disable Video (faster)");
			$("#copy").text("Copy last played video URL")
			video = true;
			//setCookie("videoPref", "true");
		}

	};

	//loop toggle
	$("#loopToggle").click(function() {
		if (loop) {//turn off loop
			$(this).addClass('button-primary').text('Enable Looping (video only)');
			$("#video").removeAttr('loop');
			loop = false;
		}
		else {//turn on loop
			$(this).removeClass('button-primary').text('Disable Looping (video only)');
			$("#video").attr("loop", "loop");
			if (shown) {
				$("#video")[0].play();
			}
			loop = true;
		}
	});

	// enable dark theme
	$("head").append('<link rel="stylesheet" href="../css/dark.css" id="dark">');
	//initial background change
	changeBg();
	//start background loop
	setInterval(changeBg, 10000);
	$("#githubLogo").attr("src", "../images/GitHub-Mark-Light-32px.png");
	//change background
	function changeBg(){
		$("#bgWrapper").fadeOut(500, 'easeOutCubic', function() {
			//replace image after fadeout
			$("#bg").attr({
				style: randomBg()
			});
		});
		//fade back in
		$("#bgWrapper").fadeIn(1500, 'easeOutCubic');
	};

	//dark theme toggler - depreciated
	$("#darkToggle").click(function() {
		if ( $("#dark").length !== 0 ) {//disable darkmode
			//remove dark.css
			$("#dark").remove();
			//switch to dark github logo
			$("#githubLogo").attr("src", "../images/GitHub-Mark-32px.png");
			$("#bgWrapper").hide();
			//change button text
			$(this).addClass('button-primary').text('Enable Dark Theme');
			
		}
		else {//enable dark mode
			//add dark.css
			$("head").append('<link rel="stylesheet" href="../css/dark.css" id="dark">');
			//change to light github logo
			//$("#githubLogo").attr("src", "../images/GitHub-Mark-Light-32px.png");
			//change background
			//$("#bg").attr({
			//	style: randomBg()
			//});
			//initial background change
			changeBg();
			if (dark == false) {// if loop has not started yet
				//start background loop
				setInterval(changeBg, 10000);
				dark = true;
			}
			//change button text
			$(this).removeClass('button-primary').text('Disable Dark Theme');
		}
	});

	//change background
	function changeBg(){
		$("#bgWrapper").fadeOut(500, 'easeOutCubic', function() {
			//replace image after fadeout
			$("#bg").attr({
				style: randomBg()
			});
		});
		//fade back in
		$("#bgWrapper").fadeIn(1500, 'easeOutCubic');
	};

	//generate random path to image
	function randomBg() {
		var backgrounds = [
			"chaeyoung1.jpg",
			"chaeyoung2.jpg",
			"chaeyoung3.jpg",
			"chaeyoung4.jpg",
			"chaeyoung5.jpg",
			"dahyun1.jpg",
			"dahyun2.jpg",
			"dahyun3.jpg",
			"jeongyeon1.jpg",
			"jihyo1.jpg",
			"jihyo2.jpg",
			"mina1.jpg",
			"mina2.jpg",
			"momo1.jpg",
			"nayeon1.jpg",
			"nayeon2.jpg",
			"nayeon3.jpg",
			"sana1.jpg",
			"sana2.jpg",
			"sana3.jpg",
			"twice1.jpg",
			"twice2.jpg",
			"tzuyu1.jpg",
			"tzuyu2.jpg",
			"tzuyu3.jpg",
			"jtrinity1.jpg"
		];
		var background = backgrounds[Math.floor(Math.random()*backgrounds.length)];
		var backgroundImg = "background-image: url(../images/bg/" + background + ")";
		return backgroundImg;
	};

	$("#suggestionsButton").click(function() {
		$("#suggestions").slideToggle(750, 'easeInOutCubic');
		var content = $(this).text();
		if (content == "Want stuff added?") {
			$(this).text('Close');
		}
		else {
			$(this).text('Want stuff added?');
		}
	});


	//copy to clipboard
	new Clipboard('#copy', {
	    text: function(trigger) {
	    	url = "https://2wice.net" + lastPlayed.substring(2)
	    	var encodedPath = url.replace(/ /g,"%20")
	    	if (video) {
				return encodedPath;
	    	}
	    	else {
	    		mp3Url = encodedPath.replace("mp4", "mp3")
	    		return mp3Url
	    	}
	    }
	});

	//COOKIES - fk this shit lol i gave up
	//check if cookies, if none set to false
	/*
	function checkCookie() {
		//console.log("cookie check");
		var videoPref = getCookie("videoPref");
		if (videoPref != "") {
			//console.log("cookies found");
			video = videoPref;
			console.log(video);
			videoToggle();
		}
		else {
			//console.log("cookies not found");
			//default disable video
			video = false;
		}

	};

	//set cookie
	function setCookie(cname, cvalue) {
		//console.log(path);
		console.log("cookie set " + cvalue);
	    document.cookie = cname + "_" + path + "=" + cvalue;
	};

	//get cookie (  copypasta :(  )
	function getCookie(cname) {
	    var name = cname + "_" + path + "=";
	    var ca = document.cookie.split(';');
	    console.log(ca);
	    for(var i=0; i<ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1);
	        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
	    }
	    return "";
	};
	*/
});
