$(document).ready(function() {
	$("img").click(function() {
		//reload image on click
		var src = $(this).attr("src");
		//reload image - slight loop workaround
		$(this).attr("src", src);
		//get sound id
		var id = $(this).attr("id");
		//build path
		var path = "./audio/0" + id + ".mp3";
		//create new audio
		var audioFile = new Audio(path);
		//play audio
		audioFile.play();
		});
	});